# Kaniko Build CI/CD component

A pipeline component to build docker images and push them to a registry.

## Individual jobs

You can add jobs in this component to an existing `.gitlab-ci.yml` file by using the `include:` keyword.

```
include:
  - component: https://gitlab.hpc.ut.ee/ci-cd-components/kaniko-build/prepare-extra-args@VERSION
      inputs:
        kaniko_build_args: <YOUR SPACE SEPARATED BUILD ARGUMENTS>
  - component: https://gitlab.hpc.ut.ee/ci-cd-components/kaniko-build/kaniko-build-mr@VERSION
      inputs:
        image_name: gcr.io/kaniko-project/executor:debug
        image_entrypoint: ""
        stage: build
        kaniko_extra_args: ""
  - component: https://gitlab.hpc.ut.ee/ci-cd-components/kaniko-build/kaniko-build@VERSION
      inputs:
        image_name: gcr.io/kaniko-project/executor:debug
        image_entrypoint: ""
        stage: build
        kaniko_extra_args: ""
  - component: https://gitlab.hpc.ut.ee/ci-cd-components/kaniko-build/kaniko-build-external-registry@VERSION
      inputs:
        image_name: gcr.io/kaniko-project/executor:debug
        image_entrypoint: ""
        stage: build
        kaniko_extra_args: ""
        registry_address: gcr.io
        registry_path: my_image
```

where `<VERSION>` is a version number of some release of this component.

### Inputs

**prepare-extra-args** generates a string (`--build-arg=<YOUR BUILD ARG>`) for each build argument to add to the builder.

| Input               | Default value | Description                                                                                                |
| --------------------|:-------------:| ----------------------------------------------------------------------------------------------------------:|
| `kaniko_build_args` | `""`          | A space separated list of build arguments to add to kaniko builder as a value for `--build-arg` argument.  |
| `only`              | `[branches]   | Rukes to use limiting when to run a job. Input mustt be an array. for more info about the `only` keyword, see https://docs.gitlab.com/ee/ci/yaml/#only--except |

**kaniko-build-mr** runs the build on a merge request and pushes it to the container registry of yout project under /mr

| Input               | Default value                          | Description                                                                                                |
| --------------------|:--------------------------------------:| ----------------------------------------------------:|
| `image_name`        | `gcr.io/kaniko-project/executor:debug` | Name of the image of the container to run the job in |
| `image_entrypoint`  | `""`                                   | Entrypoint for the container                         |
| `stage`             | `build`                                | Stage name to use for the job.                       |
| `dockerfile`        | `$CI_PROJECT_DIR/Dockerfile`           | Location of the dockerfile to build.                 |
| `kaniko_extra_args` | `""`                                   | Extra arguments to pass to kaniko builder. For more info, see: https://github.com/GoogleContainerTools/kaniko?tab=readme-ov-file#additional-flags |

**kaniko-build** runs the build on `main` or `master` branch and pushes it to the container registry of your project.

| Input               | Default value                          | Description                                                                                                |
| --------------------|:--------------------------------------:| ----------------------------------------------------:|
| `image_name`        | `gcr.io/kaniko-project/executor:debug` | Name of the image of the container to run the job in |
| `image_entrypoint`  | `""`                                   | Entrypoint for the container                         |
| `stage`             | `build`                                | Stage name to use for the job.                       |
| `dockerfile`        | `$CI_PROJECT_DIR/Dockerfile`           | Location of the dockerfile to build.                 |
| `kaniko_extra_args` | `""`                                   | Extra arguments to pass to kaniko builder. For more info, see: https://github.com/GoogleContainerTools/kaniko?tab=readme-ov-file#additional-flags |
| `only`              | `[branches]   | Rukes to use limiting when to run a job. Input mustt be an array. for more info about the `only` keyword, see https://docs.gitlab.com/ee/ci/yaml/#only--except |

**kaniko-build-external-registry** runs the build on `main` or `master` branch and pushes it to an external registry. Expects $EXTERNAL_CONTAINER_REGISTRY_USER and $EXTERNAL_CONTAINER_REGISTRY_PASSWORD CI/CD variables.

| Input               | Default value                          | Description                                                                                                |
| --------------------|:--------------------------------------:| -----------------------------------------------------------------------------------------------------------------:|
| `image_name`        | `gcr.io/kaniko-project/executor:debug` | Name of the image of the container to run the job in                                                              |
| `image_entrypoint`  | `""`                                   | Entrypoint for the container                                                                                      |
| `stage`             | `build`                                | Stage name to use for the job.                                                                                    |
| `kaniko_extra_args` | `""`                                   | Extra arguments to pass to kaniko builder. For more info, see: https://github.com/GoogleContainerTools/kaniko?tab=readme-ov-file#additional-flags |
| `registry_address`  | `"$EXTERNAL_CONTAINER_REGISTRY"`       | Address of the registry to push the image to. Default value uses a CI/CD variable, the use of which is deprecated |
| `registry_path`     | `"$EXTERNAL_CONTAINER_REGISTRY_PATH"`  | Path in the registry to push the image to. Default value uses a CI/CD variable, the use of which is deprecated    |
| `dockerfile`        | `$CI_PROJECT_DIR/Dockerfile`           | Location of the dockerfile to build.                                                                              |
| `only`              | `[branches]   | Rukes to use limiting when to run a job. Input mustt be an array. for more info about the `only` keyword, see https://docs.gitlab.com/ee/ci/yaml/#only--except |


